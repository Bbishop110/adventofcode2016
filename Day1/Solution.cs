﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1
{
    public class Solution
    {
        public static void Run()
        {
            var person = new PersonLocation();
            foreach (var instruction in Instructions.Steps)
            {
                person.WalkByBlock(new ParseInstruction(instruction));
            }
            var locations = person.VisitedLocations;
            var distance = Math.Abs(person.Location.X) + Math.Abs(person.Location.Y);
            var distance2 = Math.Abs(person.VisitedTwice.First().X) + Math.Abs(person.VisitedTwice.First().Y);
            Console.WriteLine(string.Format("Final location is {0}, {1}. Distance is {2}. First visited {3},{4} twice, which is {5} blocks away", person.Location.X, person.Location.Y, distance,person.VisitedTwice.First().X, person.VisitedTwice.First().Y,distance2));
        }
    }
}
