﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2
{
    public class TwelveKeyPad : IKeypad
    {

        private bool OnTopRow(int currentButton)
        {
            return (currentButton == 1 || currentButton == 2 || currentButton == 4 || currentButton == 5 || currentButton == 9);
        }

        private bool OnBottomRow(int currentButton)
        {
            return (currentButton == 5 || currentButton == 10 || currentButton == 13 || currentButton == 12 || currentButton == 9);
        }

        private bool OnLeftColumn(int currentButton)
        {
            return (currentButton == 1 || currentButton == 2 || currentButton == 5 || currentButton == 10 || currentButton == 13);
        }

        private bool OnRightColumn(int currentButton)
        {
            return (currentButton == 1 || currentButton == 4 || currentButton == 9 || currentButton == 12 || currentButton == 13);
        }

        public int Navigate(char direction, int currentButton)
        {
            int newPosition = currentButton;
            switch (direction)
            {
                case ('U'):
                    if (!OnTopRow(currentButton))
                    {
                        if(currentButton == 13 || currentButton == 3)
                            newPosition = currentButton - 2;
                        else
                            newPosition = currentButton - 4;
                    }
                    break;
                case ('D'):
                    if (!OnBottomRow(currentButton))
                    {
                        if (currentButton == 1 || currentButton == 11)
                            newPosition = currentButton + 2;
                        else
                            newPosition = currentButton + 4;
                    }
                    break;
                case ('L'):
                    if (!OnLeftColumn(currentButton))
                    {
                        newPosition = currentButton - 1;
                    }
                    break;
                case ('R'):
                    if (!OnRightColumn(currentButton))
                        newPosition = currentButton + 1;
                    break;
            }
            return newPosition;
        }
    }
}
