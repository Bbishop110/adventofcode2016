﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day9
{
    class Solve
    {
        string _decompressedData = "";
        public Solve()
        {
            Part1();
            Part2();
        }
        private void Part1()
        {
            for(int i = 0; i < Instructions.Input.Length; i++)
            {
                var letter = Instructions.Input[i].ToString();
                var commandString = "";
                if (letter == "(")
                {
                    commandString = letter;
                    var nextLetter = "";
                    while (commandString.Last() != ')')
                    {
                        commandString += nextLetter;
                        nextLetter = Instructions.Input[++i].ToString();
                    }
                    var decompressor = new DecompressionCommand(commandString);
                    _decompressedData += Decompress(decompressor, i);
                    i += (decompressor.NumberOfChars-1);
                }
                else
                {
                    _decompressedData += letter;
                }
            }
            Console.WriteLine("The Part 1 decompressed length is {0}", _decompressedData.Length);
        }
        private void Part2()
        {
            // first pass should equal 1064
            // total is 10762972461
            //Dictionary<DecompressionCommand, int> commands = new Dictionary<DecompressionCommand, int>();
            //ConcurrentBag<long> lengths = new ConcurrentBag<long>();
            long decompressedLength = 0;
            for (int i = 0; i < Instructions.Input.Length; i++)
            {
                var letter = Instructions.Input[i].ToString();
                var commandString = "";
                if (letter == "(")
                {
                    commandString = letter;
                    var nextLetter = "";
                    while (commandString.Last() != ')')
                    {
                        commandString += nextLetter;
                        nextLetter = Instructions.Input[++i].ToString();
                    }

                    var decompressor = new DecompressionCommand(commandString);
                    decompressedLength += RecursiveDecompress(decompressor, i);
                    //commands.Add(decompressor, i);
                    i += (decompressor.NumberOfChars - 1);
                }
                else
                {
                    decompressedLength += 1;
                }
            }
            //Parallel.ForEach(commands, command =>
            //{
            //    lengths.Add(RecursiveDecompress(command.Key, command.Value));
            //});
            //foreach (var length in lengths)
            //{
            //    decompressedLength += length;
            //}
            Console.WriteLine("The Part 2 decompressed length is {0}", decompressedLength);

        }

        private string Decompress(DecompressionCommand command, int decompressionStartIndex)
        {
            var decompressionString = Instructions.Input.Substring(decompressionStartIndex, command.NumberOfChars);
            var decompressedString = "";
            for (int i = 0; i < command.Multiplier; i++)
            {
                decompressedString = decompressedString + decompressionString;
            }
            return decompressedString;
        }

        private string Decompress(DecompressionCommand command, string input)
        { 
            var decompressionString = input.Substring(0, command.NumberOfChars);
            var decompressedString = "";
            for (int i = 0; i < command.Multiplier; i++)
            {
                decompressedString = decompressedString + decompressionString;
            }
            return decompressedString;
        }

        private long RecursiveDecompress(DecompressionCommand command, int decompressionStartIndex)
        {
            long totalLength = 0;
            Queue<string> decompressionCommands = new Queue<string>();
            var rgx = new Regex(Regex.Escape(command.InputString));
            var inputString = rgx.Replace(Instructions.Input.Substring(decompressionStartIndex), "", 1);
            decompressionCommands.Enqueue(Decompress(command, inputString));
            while (decompressionCommands.Any())
            {
                var decompCommandText = decompressionCommands.Dequeue();
                var newCommand = GetDecompressionCommand(decompCommandText);
                if (newCommand != null)
                {
                    rgx = new Regex(Regex.Escape(newCommand.InputString));
                    decompCommandText = rgx.Replace(decompCommandText, "", 1);
                    decompressionCommands.Enqueue(decompCommandText.Substring(newCommand.NumberOfChars));
                    decompressionStartIndex += (newCommand.InputString.Length + newCommand.NumberOfChars);
                    decompressionCommands.Enqueue(Decompress(newCommand, decompCommandText));
                }
                else
                {

                    totalLength += decompCommandText.Length;
                }
            }
            return totalLength;
        }

        private DecompressionCommand GetDecompressionCommand(string input)
        {
            if (!input.Contains("("))
                return null;
            for (int i = 0; i < input.Length; i++)
            {
                var letter = input[i].ToString();
                var commandString = "";
                if (letter == "(")
                {
                    commandString = letter;
                    var nextLetter = "";
                    while (commandString.Last() != ')')
                    {
                        commandString += nextLetter;
                        nextLetter = input[++i].ToString();
                    }
                    return new DecompressionCommand(commandString);
                }
            }
            return null;
        }
    }

}
