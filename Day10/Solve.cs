﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10
{
    class Solve
    {
        List<Bot> BotList = new List<Bot>();
        List<Command> CommandListHigh = new List<Command>();
        List<Command> CommandListLow = new List<Command>();
        List<Command> CommandListValue = new List<Command>();
        List<int> Outbox = new List<int>();
        Queue<int> WorkQueue = new Queue<int>();
        string logit = "";
        public Solve()
        {
            Part1();
            Part2();
        }

        private void Part1()
        {
            PopulateBotList();
            BuildCommands();
            //foreach (var bot in BotList)
            //{
            //    bot.HighCommand = CommandListHigh.FirstOrDefault(c => c.SourceId == bot.Id);
            //    bot.LowCommand = CommandListLow.FirstOrDefault(c => c.SourceId == bot.Id);
            //    var valueCommand = CommandListValue.FirstOrDefault(c => c.DestinationId == bot.Id);
            //    if(valueCommand != null)
            //        bot.ReceiveChip(valueCommand.SourceId);
            //}
            foreach (var command in CommandListHigh)
            {
                BotList[command.SourceId].HighCommand = command;
            }
            foreach (var command in CommandListLow)
            {
                BotList[command.SourceId].LowCommand = command;
            }
            foreach (var command in CommandListValue)
            {
                BotList[command.DestinationId].ReceiveChip(command.SourceId);
            }
            List<Bot> fullBots = new List<Bot>();
            fullBots = BotList.Where(b => b.HighChip != null && b.LowChip != null).ToList();
            WorkQueue.Enqueue(Convert.ToInt32(fullBots[0].Id));
            while(WorkQueue.Count > 0)
                ExecuteCommands();
            //Console.WriteLine(logit);
        }

        private void Part2()
        {
            
        }

        private void PopulateBotList()
        {
            for (int i = 0; i < 210; i++)
            {
                BotList.Add(new Bot(i));
            }
        }

        private void BuildCommands()
        {
            foreach (var command in Instructions.commands)
            {
                if (command.StartsWith("bot"))
                {
                    CommandListHigh.Add(CommandFactory.GetInstance().GetCommand(command, "High"));
                    CommandListLow.Add(CommandFactory.GetInstance().GetCommand(command, "Low"));
                }
                else
                {
                    CommandListValue.Add(CommandFactory.GetInstance().GetCommand(command, "Value"));
                }
            }
        }

        private void ExecuteCommands()
        {
            var workerBotId = WorkQueue.Dequeue();
            if (BotList[workerBotId].HighCommand.Destination == DestinationType.Bot)
            {
                BotList[BotList[workerBotId].HighCommand.DestinationId].ReceiveChip(Convert.ToInt32(BotList[workerBotId].HighChip));
                BotList[workerBotId].HighChip = null;
            }
            else
            {
                Outbox.Add(Convert.ToInt32(BotList[workerBotId].HighChip));
                BotList[workerBotId].HighChip = null;
            }
            if (BotList[workerBotId].LowCommand.Destination == DestinationType.Bot)
            {
                BotList[BotList[workerBotId].LowCommand.DestinationId].ReceiveChip(Convert.ToInt32(BotList[workerBotId].LowChip));
                BotList[workerBotId].LowChip = null;
            }
            else
            {
                Outbox.Add(Convert.ToInt32(BotList[workerBotId].LowChip));
                BotList[workerBotId].LowChip = null;
            }
            List<Bot> fullBots = BotList.Where(b => b.HighChip != null && b.LowChip != null).ToList();
            foreach (var fullBot in fullBots)
            {
                if (fullBot.HighChip == 61 && fullBot.LowChip == 17)
                    Console.WriteLine(string.Format("Bot number {0} is comparing chip 61 and 17", fullBot.Id));
                WorkQueue.Enqueue(Convert.ToInt32(fullBot.Id));
            }
        }
    }
}
