﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10
{
    enum DestinationType
    {
        Bot,
        Output
    }
    enum SourceType
    {
        Value,
        Bot
    }
    class Command
    {
        public DestinationType Destination { get; set; }
        public int DestinationId { get; set; }
        public SourceType Source { get; set; }
        public int SourceId { get; set; }
    }
}
