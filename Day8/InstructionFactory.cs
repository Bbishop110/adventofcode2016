﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day8
{
    class InstructionFactory
    {
        private static InstructionFactory _instance;

        public static InstructionFactory GetInstance()
        {
            if(_instance == null)
                _instance = new InstructionFactory();
            return _instance;
        }

        public ModifyInstruction GetInstruction(string instruction)
        {
            var instructionParts = instruction.Split(' ');
            switch (instructionParts[0])
            {
                case("rect"):
                    return new ModifyInstruction
                    {
                        Command = instructionParts[0],
                        X = Convert.ToInt32(instructionParts[1].Split('x')[0]),
                        Y = Convert.ToInt32(instructionParts[1].Split('x')[1])
                    };
                case ("rotate"):
                    int x = 0;
                    int y = 0;
                    if ("row".Equals(instructionParts[1]))
                    {
                        y = Convert.ToInt32(instructionParts[2].Split('=')[1]);
                    }
                    else
                    {
                        x = Convert.ToInt32(instructionParts[2].Split('=')[1]);
                    }
                    return new ModifyInstruction
                    {
                        Command = instructionParts[0],
                        Section = instructionParts[1],
                        X = x,
                        Y = y,
                        Distance = Convert.ToInt32(instructionParts.Last())
                    };
                default:
                    return null;
            }
        }
    }
}
