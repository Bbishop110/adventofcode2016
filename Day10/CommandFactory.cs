﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10
{
    class CommandFactory
    {
        private static CommandFactory _instance = null;
        public static CommandFactory GetInstance()
        {
            if(_instance == null)
                _instance = new CommandFactory();
            return _instance;
        }

        public Command GetCommand(string commandText, string value)
        {
            var commandElements = commandText.Split(' ');
            DestinationType destination;
            int destId;
            SourceType source = commandElements[0] == "bot" ? SourceType.Bot : SourceType.Value;
            switch (value)
            {
                case ("Low"):
                    destination = commandElements[5] == "bot" ? DestinationType.Bot : DestinationType.Output;
                    destId = Convert.ToInt32(commandElements[6]);
                    return new Command
                    {
                        Destination = destination,
                        Source = source,
                        DestinationId = destId,
                        SourceId = Convert.ToInt32(commandElements[1])
                    };
                case ("High"):
                    destination = commandElements[10] == "bot" ? DestinationType.Bot : DestinationType.Output;
                    destId = Convert.ToInt32(commandElements[11]);
                    return new Command
                    {
                        Destination = destination,
                        Source = source,
                        DestinationId = destId,
                        SourceId = Convert.ToInt32(commandElements[1])
                    };
                case("Value"):
                    destination = commandElements[4] == "bot" ? DestinationType.Bot : DestinationType.Output;
                    return new Command
                    {
                        Destination = destination,
                        Source = source,
                        DestinationId = Convert.ToInt32(commandElements[5]),
                        SourceId = Convert.ToInt32(commandElements[1])
                    };
                default:
                    return null;
            }

            return null;
        }
    }
}
