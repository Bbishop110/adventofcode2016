﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2
{
    public class NineKeyPad : IKeypad
    {

        private bool OnTopRow(int currentButton)
        {
            return (currentButton == 1 || currentButton == 2 || currentButton == 3);
        }

        private bool OnBottomRow(int currentButton)
        {
            return (currentButton == 7 || currentButton == 8 || currentButton == 9);
        }

        private bool OnLeftColumn(int currentButton)
        {
            return (currentButton == 1 || currentButton == 4 || currentButton == 7);
        }

        private bool OnRightColumn(int currentButton)
        {
            return (currentButton == 3 || currentButton == 6 || currentButton == 9);
        }

        public int Navigate(char direction, int currentButton)
        {
            int newPosition = currentButton;
            switch (direction)
            {
                case ('U'):
                    if (!OnTopRow(currentButton))
                        newPosition = currentButton - 3;
                    break;
                case ('D'):
                    if (!OnBottomRow(currentButton))
                        newPosition = currentButton + 3;
                    break;
                case ('L'):
                    if (!OnLeftColumn(currentButton))
                        newPosition = currentButton - 1;
                    break;
                case ('R'):
                    if (!OnRightColumn(currentButton))
                        newPosition = currentButton + 1;
                    break;
            }
            return newPosition;
        }
    }
}
