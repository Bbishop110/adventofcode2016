﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2
{
    public interface IKeypad
    {
        int Navigate(char direction, int currentButton);
    }
}
