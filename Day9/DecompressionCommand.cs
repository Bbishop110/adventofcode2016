﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day9
{
    public class DecompressionCommand
    {
        public int NumberOfChars { get; private set; }
        public int Multiplier { get; private set; }
        public string InputString { get; private set; }
        public DecompressionCommand(string command)
        {
            command = command.Trim(new[] {'(', ')'});
            var commandPieces = command.Split('x');
            NumberOfChars = Convert.ToInt32(commandPieces[0]);
            Multiplier = Convert.ToInt32(commandPieces[1]);
            InputString = string.Format("({0})",command);
        }
    }
}
