﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3
{
    class Solve
    {
        public List<Shape> PossibleTriangle = new List<Shape>();
        public List<Shape> ImpossibleTriangle = new List<Shape>();
        public Solve()
        {
            Part1();
            PossibleTriangle.Clear();
            ImpossibleTriangle.Clear();
            Part2();
        }
        private void Part1()
        {
            foreach (var set in Instructions.SideMeasurements)
            {
                var shape = new Shape(set);
                if (shape.Possible)
                    PossibleTriangle.Add(shape);
                else
                    ImpossibleTriangle.Add(shape);
            }
            Console.WriteLine("There are {0} Possible and {1} Impossible Triangles", PossibleTriangle.Count, ImpossibleTriangle.Count);

        }
        private void Part2()
        {
            for (int i = 0; i+2<Instructions.SideMeasurements.Count;i+=3)
            {
                var sideSet1 = string.Format("{0}|{1}|{2}", Instructions.SideMeasurements[i].Split('|')[0], Instructions.SideMeasurements[i + 1].Split('|')[0], Instructions.SideMeasurements[i + 2].Split('|')[0]);
                var sideSet2 = string.Format("{0}|{1}|{2}", Instructions.SideMeasurements[i].Split('|')[1], Instructions.SideMeasurements[i + 1].Split('|')[1], Instructions.SideMeasurements[i + 2].Split('|')[1]);
                var sideSet3 = string.Format("{0}|{1}|{2}", Instructions.SideMeasurements[i].Split('|')[2], Instructions.SideMeasurements[i + 1].Split('|')[2], Instructions.SideMeasurements[i + 2].Split('|')[2]);
                var shapes = new List<Shape>();
                shapes.Add(new Shape(sideSet1));
                shapes.Add(new Shape(sideSet2));
                shapes.Add(new Shape(sideSet3));
                foreach (var shape in shapes)
                {
                    if (shape.Possible)
                        PossibleTriangle.Add(shape);
                    else
                        ImpossibleTriangle.Add(shape);
                }
            }
            Console.WriteLine("There are {0} Possible and {1} Impossible Triangles", PossibleTriangle.Count, ImpossibleTriangle.Count);

        }
    }
}
