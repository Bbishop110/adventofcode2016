﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SolutionRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("*** Day1 ***");
            Day1.Solution.Run();
            Console.WriteLine("*** Day2 ***");
            Day2.Solution.Run();
            Console.WriteLine("*** Day3 ***");
            Day3.Solution.Run();
            Console.WriteLine("*** Day4 ***");
            Day4.Solution.Run();
            Console.WriteLine("*** Day5 ***");
            Day5.Solution.Run();
            Console.WriteLine("*** Day6 ***");
            Day6.Solution.Run();
            Console.WriteLine("*** Day7 ***");
            //Day7.Solution.Run();
            Console.WriteLine("*** Day8 ***");
            Day8.Solution.Run();
            Console.WriteLine("*** Day9 ***");
            //Day9.Solution.Run();
            Console.WriteLine("*** Day10 ***");
            Day10.Solution.Run();
            Console.ReadLine();
        }
    }
}
