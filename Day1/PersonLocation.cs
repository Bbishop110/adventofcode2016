﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Day1
{
    public enum Directions
    {
        North,
        East,
        South,
        West
    };
    public class PersonLocation
    {
        public Directions Facing;
        public Grid Location = new Grid();
        public List<Point> VisitedLocations = new List<Point>();
        public List<Point> VisitedTwice = new List<Point>();
        public void Walk(ParseInstruction instruction)
        {
            UpdateFacingDirection(instruction.Turn);
            switch (Facing)
            {
                case (Directions.North):
                    Location.Y += instruction.Blocks;
                    break;
                case (Directions.East):
                    Location.X += instruction.Blocks;
                    break;
                case (Directions.South):
                    Location.Y -= instruction.Blocks;
                    break;
                case (Directions.West):
                    Location.X -= instruction.Blocks;
                    break;
            }
            //VisitedLocations.Add(new Point(Location.X, Location.Y));

        }
        public void WalkByBlock(ParseInstruction instruction)
        {
            UpdateFacingDirection(instruction.Turn);
            Point newPoint;
            switch (Facing)
            {
                case (Directions.North):
                    for(int i = 0; i<instruction.Blocks;i++)
                    {
                        CheckIfVisited(new Point(Location.X, Location.Y + i));
                    }
                    Location.Y += instruction.Blocks;
                    break;
                case (Directions.East):
                    for (int i = 0; i < instruction.Blocks; i++)
                    {
                        CheckIfVisited(new Point(Location.X + i, Location.Y));
                    }
                    Location.X += instruction.Blocks;
                    break;
                case (Directions.South):
                    for (int i = 0; i < instruction.Blocks; i++)
                    {
                        CheckIfVisited(new Point(Location.X, Location.Y - i));
                    }
                    Location.Y -= instruction.Blocks;
                    break;
                case (Directions.West):
                    for (int i = 0; i < instruction.Blocks; i++)
                    {
                        CheckIfVisited(new Point(Location.X - i, Location.Y));
                    }
                    Location.X -= instruction.Blocks;
                    break;
            }

        }
        private void UpdateFacingDirection(string turn)
        {
            int direction = Convert.ToInt32(Facing);
            if (turn == "R")
                ++direction;
            if (turn == "L")
                --direction;
            switch (direction)
            {
                case 4:
                    direction = 0;
                    break;
                case -1:
                    direction = 3;
                    break;
            }
            Facing = (Directions)direction;
        }

        private bool HaveWeBeenHere(Point location)
        {
            return VisitedLocations.Contains(location);
        }

        private void CheckIfVisited(Point newPoint)
        {
            if (HaveWeBeenHere(newPoint))
            {
                VisitedTwice.Add(newPoint);
            }
            VisitedLocations.Add(newPoint);
        }
    }
}
