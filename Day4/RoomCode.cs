﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day4
{
    enum Characters
    {
        a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z
    }
    public class RoomCode
    {
        public string Hash { get; private set; }
        public string Checksum { get; private set; }
        public string PlainText { get; private set; }
        public bool Valid { get; private set; }
        public int SectorId { get; private set; }

        public RoomCode(string encryptedData)
        {
            var encryptionPieces = encryptedData.Split('[');
            Checksum = encryptionPieces[1].Trim(']');
            SectorId = Convert.ToInt32(encryptionPieces[0].Split('-').Last());
            Hash = encryptionPieces[0].Replace(string.Format("-{0}", SectorId), "");
            Valid = isValid();
            if(Valid)
            {
                PlainText = GetPlainText();
            }
        }
        private bool isValid()
        {
            var hash = Hash.Replace("-", "");
            List<Tuple<char, int>> letterResults = new List<Tuple<char, int>>();
            foreach (char letter in hash)
            {
                int count = hash.Count(f => f == letter);
                bool containsLetter = false;
                foreach(var letterFreq in letterResults)
                {
                    if (letterFreq.Item1 == letter)
                    {
                        containsLetter = true;
                        break;
                    }
                }
                if(!containsLetter)
                    letterResults.Add(new Tuple<char, int>(letter, count));
            }
            
            var checksumLetters = letterResults.OrderByDescending(x => x.Item2).ThenBy(x => x.Item1).ToList();
            string expectedChecksum = string.Format("{0}{1}{2}{3}{4}", checksumLetters[0].Item1, checksumLetters[1].Item1, checksumLetters[2].Item1, checksumLetters[3].Item1, checksumLetters[4].Item1);

            return expectedChecksum == Checksum;
        }
        private string GetPlainText()
        {
            string plainText = "";
            foreach(var letter in Hash)
            {
                plainText += decrypt(letter.ToString());
            }
            return plainText;
        }
        private string decrypt(string character)
        {
            string decryptedCharacter;
            if (character == "-")
            {
                if (SectorId % 2 != 0)
                    return " ";
                else
                    return "-";
            }
            var enumChar = (Characters)Enum.Parse(typeof(Characters), character);
            for (int i = 0; i <SectorId;i++)
            {
                switch (enumChar)
                {
                    case (Characters.z):
                        enumChar = Characters.a;
                        break;
                    default:
                        enumChar++;
                        break;
                }
            }
            return enumChar.ToString();
        }
    }
}
