﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day8
{
    public class ModifyInstruction
    {
        public string Command { get; set; }
        public string Section { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Distance { get; set; }
    }
}
