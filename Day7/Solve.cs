﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day7
{
    public class Solve
    {
        public Solve()
        {
            Part1();
            Part2();
        }

        private void Part1()
        {
            var TlsSupportingIps = new List<string>();
            foreach (var ipAddress in Instructions.IpAddressList)
            {
                if(SupportsTls(ipAddress))
                    TlsSupportingIps.Add(ipAddress);
            }
            Console.WriteLine("{0} IP addresses support transport-layer snooping",TlsSupportingIps.Count);
        }

        private void Part2()
        {
            var SslSupportingIps = new List<string>();
            foreach (var ipAddress in Instructions.IpAddressList)
            {
                if (SupportsSsl(ipAddress))
                    SslSupportingIps.Add(ipAddress);
            }
            Console.WriteLine("{0} IP addresses support super-secret listening", SslSupportingIps.Count);

        }

        private bool SupportsTls(string ipAddress)
        {
            var hypernetSequences = GetHypernetSequences(ipAddress);
            foreach (var hypernetSequence in hypernetSequences)
            {
                if (HasABBA(hypernetSequence))
                    return false;
            }
            var nonHypernetSequences = GetNonHypernetSequences(ipAddress);
            foreach (var nonHypernetSequence in nonHypernetSequences)
            {
                if (HasABBA(nonHypernetSequence))
                {
                    return true;
                }
            }

            return false;
        }

        private bool SupportsSsl(string ipAddress)
        {
            var hypernetSequences = GetHypernetSequences(ipAddress);
            var hypernetBABkeys = new List<string>();
            foreach (var hypernetSequence in hypernetSequences)
            {
                foreach (var babSequence in GetBAB(hypernetSequence))
                {
                    hypernetBABkeys.Add(babSequence);
                }
            }
            var nonHypernetSequences = GetNonHypernetSequences(ipAddress);
            foreach (var nonHypernetSequence in nonHypernetSequences)
            {
                if (HasABA(nonHypernetSequence, hypernetBABkeys))
                {
                    return true;
                }
            }

            return false;
        }

        private bool HasABBA(string input)
        {
            var mc = Regex.Matches(input, @"(.)\1{1,}");
            foreach (var match in mc)
            {
                int matchLocation = input.IndexOf(match.ToString());
                try
                {
                    var previousLetter = input.Substring(matchLocation - 1, 1);
                    var nextLetter = input.Substring(matchLocation + 2, 1);
                    var doubledLetterInMiddle = match.ToString()[0].ToString();
                    if ((previousLetter == nextLetter) && previousLetter != doubledLetterInMiddle)
                    {
                        return true;
                    }

                }
                catch (System.ArgumentOutOfRangeException)
                {
                    // The index match of the double letter was either at the beginning or end of the string, 
                    // therefore it cannot be in ABBA format, so swallow and continue.
                }
            }
            return false;
        }
        private bool HasABA(string input, IEnumerable<string> babSequences)
        {
            foreach (var babSequence in babSequences)
            {
                var abaSequence = new string(new char[] {babSequence[1], babSequence[0], babSequence[1]});
                if (input.Contains(abaSequence))
                {
                    return true;
                }
            }
            return false;
        }
        private IEnumerable<string> GetBAB(string input)
        {
            var matches = new List<string>();
            for (int i = 0; i < input.Length - 2; i++)
            {
                var firstChar = input.ElementAt(i);
                var lastChar = input.ElementAt(i + 2);
                if (firstChar == lastChar)
                {
                    matches.Add(input.Substring(i,3));
                }
            }
            return matches;
        }

        private IEnumerable<string> GetHypernetSequences(string ipAddress)
        {
            var hypernetSequences = new List<string>();
            var matches = Regex.Matches(ipAddress, @"\[([^]]+)\]");
            foreach (var match in matches)
            {
                hypernetSequences.Add(match.ToString().Trim(new []{'[',']'}));
            }
            return hypernetSequences;
        }
        private IEnumerable<string> GetNonHypernetSequences(string ipAddress)
        {
            var matches = Regex.Matches(ipAddress, @"\[([^]]+)\]");
            foreach (var match in matches)
            {
                ipAddress = ipAddress.Replace(match.ToString(), "|");
            }
            return ipAddress.Split('|').ToList();
        }
    }
}
