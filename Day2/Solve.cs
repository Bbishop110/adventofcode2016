﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2
{
    public class Solve
    {
        public Solve(IKeypad keypad)
        {
            var code = new FindCode();
            code.PrintCombo(
                (from instruction in Instructions.CodeMap 
                 let currentButton = 5 
                 select code.GetCode(instruction, currentButton, keypad)).ToList());
        }
    }
}
