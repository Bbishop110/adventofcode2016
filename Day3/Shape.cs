﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3
{
    public class Shape
    {
        public int SideA { get; set; }
        public int SideB { get; set; }
        public int SideC { get; set; }
        public bool Possible { get; private set; }

        public Shape (string sides)
        {         
            var sidesSet = sides.Split('|');
            SideA = Convert.ToInt32(sidesSet[0]);
            SideB = Convert.ToInt32(sidesSet[1]);
            SideC = Convert.ToInt32(sidesSet[2]);
            Possible = IsTriangle();
        }
        private bool IsTriangle()
        {         
            if (SideA + SideB > SideC)
            {
                if (SideA + SideC > SideB)
                {
                    if (SideB + SideC > SideA)
                        return true;
                }
            }            
            return false;
        }
    }
}
