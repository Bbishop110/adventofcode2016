﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace Day8
{
    class Solve
    {
        string[] grid = new string[6];
        public Solve()
        {
            BuildGrid();
            Part1();
            Part2();
        }

        private void Part1()
        {
            foreach(var instruction in Instructions.instructions)
            {
                var inst = InstructionFactory.GetInstance().GetInstruction(instruction);
                ApplyInstruction(inst);
            }
            int lightsThatAreOn = 0;
            foreach (var row in grid)
            {
                lightsThatAreOn += row.Count(f => f == '#');
            }
            Console.WriteLine("There are {0} lights on.",lightsThatAreOn);

        }

        private void Part2()
        {
            for(int i = 0; i<grid.Length;i++)
            {
                grid[i] = grid[i].Replace('.', ' ');
            }
            PrintGrid();
        }

        private void ApplyInstruction(ModifyInstruction instruction)
        {            
            switch(instruction.Command)
            {
                case ("rect"):
                    // X selects column, Y selects Row
                    for (int i = 0; i < instruction.X; i++)
                    {
                        for (int j = 0; j < instruction.Y; j++)
                        {
                            var newValue = new StringBuilder(grid[j]);
                            newValue[i] = '#';
                            grid[j] = newValue.ToString();
                        }
                    }
                    break;
                case ("rotate"):
                    if ("row".Equals(instruction.Section))
                    {
                        ShiftRight(instruction);
                    }
                    if ("column".Equals(instruction.Section))
                    {
                        ShiftDown(instruction);
                    }
                    break;

            }   
        }

        private void ShiftRight(ModifyInstruction instruction)
        {
            int actualShift = instruction.Distance%50;
            for(int i = 0; i<actualShift;i++)
            {
                grid[instruction.Y] = grid[instruction.Y].Insert(0, " ");
            }
            string wrapCharacters = grid[instruction.Y].Substring(50);
            var newValue = new StringBuilder(grid[instruction.Y]);
            for (int j = 0; j < wrapCharacters.Length; j++)
            {
                newValue[j] = wrapCharacters[j];
            }
            grid[instruction.Y] = newValue.ToString();
            grid[instruction.Y] = grid[instruction.Y].Remove(50);
        }

        private void ShiftDown(ModifyInstruction instruction)
        {
            int actualShift = instruction.Distance % 6;
            string columnValues = string.Format("{0}{1}{2}{3}{4}{5}", grid[0].ElementAt(instruction.X),
                grid[1].ElementAt(instruction.X), grid[2].ElementAt(instruction.X), grid[3].ElementAt(instruction.X),
                grid[4].ElementAt(instruction.X), grid[5].ElementAt(instruction.X));
            for (int i = 0; i < actualShift; i++)
            {
                columnValues = columnValues.Insert(0, " ");
            }
            string wrapCharacters = columnValues.Substring(6);
            var newValue = new StringBuilder(columnValues);
            for (int j = 0; j < wrapCharacters.Length; j++)
            {
                newValue[j] = wrapCharacters[j];
            }
            for (int i = 0; i < grid.Length; i++)
            {
                var newRow = new StringBuilder(grid[i]);
                newRow[instruction.X] = newValue[i];
                grid[i] = newRow.ToString();
            }
        }
        private void PrintGrid()
        {
            for (int i = 0; i < grid.Length; i++)
            {
                Console.Write(string.Format("{0} ", grid[i]));
                Console.Write(Environment.NewLine + Environment.NewLine);
            }
            Console.Write(Environment.NewLine + Environment.NewLine);
        }

        private void BuildGrid()
        {
            for (int i = 0; i < 6; i++)
            {
                grid[i] = "..................................................";
            }
        }

    }
}
