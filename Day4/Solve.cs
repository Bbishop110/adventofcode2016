﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day4
{
    class Solve
    {
        List<RoomCode> ValidRooms = new List<RoomCode>();
        List<RoomCode> InvalidRooms = new List<RoomCode>();
        public Solve()
        {
            Part1();
            Part2();
        }
        private void Part1()
        {
            int sectorIDTotal = 0;
            foreach(var roomData in Instructions.RoomIds)
            {
               var room =  new RoomCode(roomData);
                if (room.Valid)
                {
                    sectorIDTotal += room.SectorId;
                    ValidRooms.Add(room);
                }
            }
            Console.WriteLine("The total valid SectorIDs equal {0}", sectorIDTotal);
            
        }
        private void Part2()
        {
            foreach (var validRoom in ValidRooms)
            {
                if (validRoom.PlainText.Contains("north"))
                {
                    Console.WriteLine("The SectorID is  {0}", validRoom.SectorId);
                }
            }
        }
    }
}
