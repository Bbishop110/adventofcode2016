﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Day2;

namespace Day2
{
    enum Buttons
    {
        ZERO,
        ONE,
        TWO,
        THREE,
        FOUR,
        FIVE,
        SIX,
        SEVEN,
        EIGHT,
        NINE,
        A,
        B,
        C,
        D

    };
    class ButtonWalk
    {
        public int MoveToNext(char direction, int currentButton, IKeypad keypad)
        {
            return keypad.Navigate(direction,currentButton);
        }
    }
}
