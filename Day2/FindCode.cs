﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2
{
    class FindCode
    {
        public int GetCode(string instructions, int location, IKeypad keypad)
        {
            return instructions.Aggregate(location, (current, instruction) => new ButtonWalk().MoveToNext(instruction, current, keypad));
        }

        public void PrintCombo(List<int> numbers)
        {
            Console.WriteLine(string.Join(" ", numbers.Select(i => (Buttons) i).ToArray()));
        }
    }
}
