﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2
{
    public class Solution
    {
        public static void Run()
        {
            new Solve(new NineKeyPad());
            new Solve(new TwelveKeyPad());
        }
    }
}
