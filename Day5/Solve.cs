﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Day5
{
    class Solve
    {
        List<string> password = new List<string>();
        List<int> knownIndices = new List<int>();
        //private List<int> knownIndicesForRerun = new List<int>();
        private List<int> knownIndicesForRerun = Instructions.knownIndices; // use to make day5 progress quickly. Required indices have been saved off
        string[] passwordArr = new string[8];
        public delegate void FoundHashEventHandler(string hash, int i);
        public event EventHandler<HashFoundArgs> HashFound;
        public Solve(string input)
        {
            Thread t = new  Thread(() => HashGenerator(input));
            t.Start();
            string animation = "|";
            int i = 1000;
            while (passwordArr.Any(element => element == null))
            {
                if (i == 1000)
                    animation = "|";
                if (i == 2000)
                    animation = "/";
                if (i == 3000)
                    animation = "-";
                if (i == 4000)
                    animation = "\\";
                if (i == 5000)
                    animation = "|";
                if (i == 6000)
                    animation = "/";
                if (i == 7000)
                    animation = "-";
                if (i == 8000)
                    animation = "\\";
                Console.Write("\rDecrypting {0}", animation);
                i++;
                if (i == 9000)
                    i = 1000;
            }
            Console.Write("\r");
            t.Join();
            t.Abort();
            Console.WriteLine("The Part 1 password is: {0}", string.Join("", password.ToArray()));
            Console.WriteLine("The Part 2 password is: {0}", string.Join("", passwordArr));
            //Part1(input);
            //Part2(input);
        }

        private void Part1(string input)
        {
            int i = 4000000;
            while (password.Count < 8)
            {
                var testInput = string.Format("{0}{1}", input, i);
                GetPasswordChar(GetMD5(testInput),i);
                i++;
            }
            Console.WriteLine("The password is: {0}", string.Join("", password.ToArray()));
            
        }


        private void Part2(string input)
        {
            bool emptyElements = true;
            int i = knownIndices.Last()+1;
            foreach (var knownIndex in knownIndices)
            {
                var testInput = string.Format("{0}{1}", input, knownIndex);
                GetPasswordPositionAndChar(GetMD5(testInput),i);
                if (passwordArr.All(element => element != null))
                    emptyElements = false;
            }
            while (emptyElements)
            {
                var testInput = string.Format("{0}{1}", input, i);
                GetPasswordPositionAndChar(GetMD5(testInput),i);
                i++;
                if (passwordArr.All(element => element != null))
                    emptyElements = false;
            }
            Console.WriteLine("The password is: {0}", string.Join("", passwordArr));

        }

        private string GetMD5(string input)
        {
            // byte array representation of that string
            byte[] encodedPassword = new UTF8Encoding().GetBytes(input);

            // need MD5 to calculate the hash
            byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);

            // string representation (similar to UNIX format)
            return BitConverter.ToString(hash)
               // without dashes
               .Replace("-", string.Empty)
               // make lowercase
               .ToLower();

            // encoded contains the hash you are wanting

        }
        private void GetPasswordChar(string hash, int i)
        {
            if (hash.StartsWith("00000"))
            {
                if(password.Count < 8)
                    password.Add(hash.ToCharArray()[5].ToString());
                knownIndices.Add(i);
            }
        }
        private void GetPasswordPositionAndChar(string hash, int i)
        {
            if (hash.StartsWith("00000"))
            {
                int outputInt;
                if(int.TryParse(hash.ToCharArray()[5].ToString(),out outputInt))
                {
                    if(outputInt < 8)
                    {
                        if(passwordArr[outputInt] == null)
                            passwordArr[outputInt] = hash.ToCharArray()[6].ToString();
                    }
                }
            }
        }

        private void HashGenerator(string input)
        {
            //string input = "uqwqemis";
            if (knownIndicesForRerun.Count > 0)
            {
                foreach (var knownIndex in knownIndicesForRerun)
                {
                    var hash = GetMD5(input + knownIndex);
                    if (hash.StartsWith("00000"))
                    {
                        HashFoundArgs e = new HashFoundArgs(hash, knownIndex);
                        HandleHashFound(this, e);
                    }

                }
            }
            else
            {
                for (int i = 4000000; i < 26400000; i++)
                {
                    var hash = GetMD5(input + i);
                    if (hash.StartsWith("00000"))
                    {
                        HashFoundArgs e = new HashFoundArgs(hash, i);
                        HandleHashFound(this, e);
                    }
                }
            }
        }

        private void HandleHashFound(object source, HashFoundArgs e)
        {
            GetPasswordChar(e.Hash, e.Index);
            GetPasswordPositionAndChar(e.Hash, e.Index);
        }

        public class HashFoundArgs
        {
            public string Hash { get; set; }
            public int Index { get; set; }

            public HashFoundArgs(string hash, int index)
            {
                this.Hash = hash;
                Index = index;

            }
        }
    }
}
