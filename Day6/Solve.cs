﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day6
{
    class Solve
    {
        public Solve()
        {
            Part1();
            Part2();
        }

        private void Part1()
        {
            //Console.WriteLine("The error-corrected message is {0}", Instructions.Codes.Aggregate("", (current, code) => current + GetMostCommonLetter(code)));
            Console.WriteLine("The error-corrected message is {0}", Instructions.Codes.Aggregate("", (current, code) => current + ProcessLetters(code).OrderByDescending(x => x.Item2).First().Item1.ToString()));
        }

        private void Part2()
        {
            //Console.WriteLine("The error-corrected message is {0}", Instructions.Codes.Aggregate("", (current, code) => current + GetLeastCommonLetter(code)));
            Console.WriteLine("The error-corrected message is {0}", Instructions.Codes.Aggregate("", (current, code) => current + ProcessLetters(code).OrderBy(x => x.Item2).First().Item1.ToString()));
        }
        private IEnumerable<Tuple<char, int>> ProcessLetters(string letters)
        {
            var letterResults = new List<Tuple<char, int>>();
            //foreach (char letter in letters.Where(letter => !ListContainsLetter(letterResults, letter)))
            foreach (var letter in letters.Where(letter => letterResults.All(letterTuple => letterTuple.Item1 != letter)))
            {
                letterResults.Add(new Tuple<char, int>(letter, letters.Count(f => f == letter)));
            }
            return letterResults;
        }

        private string GetMostCommonLetter(string input)
        {
            return ProcessLetters(input).OrderByDescending(x => x.Item2).First().Item1.ToString();
        }

        private string GetLeastCommonLetter(string input)
        {
            return ProcessLetters(input).OrderBy(x => x.Item2).First().Item1.ToString();
        }

        private bool ListContainsLetter(IEnumerable<Tuple<char, int>> list, char letter)
        {
            return list.Any(letterTuple => letterTuple.Item1 == letter);
        }
    }
}
