﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1
{
    public class ParseInstruction
    {
        public string Turn { get; set; }
        public int Blocks { get; set; }
        public ParseInstruction(string instruction)
        {
            Turn = instruction.First().ToString();
            Blocks = Convert.ToInt32(instruction.Substring(1));
        }
    }
}
