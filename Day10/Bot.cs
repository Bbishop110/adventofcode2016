﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace Day10
{
    class Bot
    {
        public int? Id { get; private set; }
        public int? LowChip { get; set; }
        public int? HighChip { get; set; }
        //public int? DestinationIdHigh { get; set; }
        //public int? DestinationIdLow { get; set; }
        //public bool? HighToBot { get; set; }
        //public bool? LowToBot { get; set; }
        public Command LowCommand { get; set; }
        public Command HighCommand { get; set; }

        public Bot(int botNum)
        {
            Id = botNum;
        }

        public void ReceiveChip(int chipNumber)
        {
            if (LowChip == null)
            {
                LowChip = chipNumber;
            }
            else if (chipNumber > LowChip)
                HighChip = chipNumber;
            else if (chipNumber < LowChip)
            {
                HighChip = LowChip;
                LowChip = chipNumber;
            }
        }

        private void Clear()
        {
            LowChip = null;
            HighChip = null;
            LowCommand = null;
            HighCommand = null;
        }
    }
}
